from git import Repo, Git
#from git import Remote
from git.exc import GitCommandError, NoSuchPathError
import os

class ProjectDir():
    def __init__(self, dir, url):
        self.m_dir = dir
        self.m_url = url
        self.m_gitlab = None
        #self.git_ssh_cmd = 'ssh -i ~/.ssh/alex76sh1 -F /dev/null'
        self.ssh_cmd = 'ssh -i id_rsa'
        self.init()
        #self.setURL(url)
        #self.update()
        
    def init(self):
        try:
            self.m_repo = Repo(self.m_dir)
            self.m_original = self.m_repo.remotes.origin
            self.m_gitlab = self.m_original
            print("Open Repo")
            return;
        except NoSuchPathError:
            print("Clonning")
            #self.m_repo = Repo.init(self.m_dir)
            with Git().custom_environment(GIT_SSH_COMMAND=self.ssh_cmd):
                self.m_repo = Repo.clone_from(self.m_url, self.m_dir)
            self.m_original = self.m_repo.remotes.origin
            self.m_gitlab = self.m_original
        
    def setURL(self, url):
        try:
        #if 'gitlab' in self.m_repo.remotes: return;
            self.m_gitlab = self.m_repo.create_remote('gitlab', url)
            self.update()
        except GitCommandError:
            pass
        #if self.m_gitlab.exists(): self.m_gitlab.fetch()
    
    def download(self):
        if not self.m_gitlab: return;
        if not self.m_gitlab.exists(): return;
        print("DownLoad")
        
        try:
            with self.m_repo.git.custom_environment(GIT_SSH_COMMAND=self.ssh_cmd):
                self.m_gitlab.fetch()
                #self.m_repo.submodule_update(recursive=True)
                return True
        except GitCommandError:
            print("Internet is down")
            return False
        
    def update(self):
        if not self.m_gitlab: return;
        if not self.m_gitlab.exists(): return;
        print("Update")
        #mst = self.m_repo.refs[branch]
        #rmst = self.m_repo.remotes["origin"].refs[branch]
        #merge_base = self.m_repo.merge_base(mst, rmst)
        #self.m_repo.index.merge_tree
        try:
            with self.m_repo.git.custom_environment(GIT_SSH_COMMAND=self.ssh_cmd):
                self.m_gitlab.pull()
                self.m_repo.submodule_update(recursive=True)
                return True
        except GitCommandError:
            print("Internet is down")
            return False

    def getBranchInfo(self, branch):
        if not self.m_repo: return None
        print("getBranchInfo")
        if not branch in self.m_repo.refs: return None
        mst = self.m_repo.refs[branch]
        rmst = self.m_repo.remotes["origin"].refs[branch]
        info = {
            "branchName": mst.name,
            "locale": {
                "author": {
                    "name": mst.commit.author,
                    "datetime": mst.commit.authored_datetime
                },
                "committer": {
                    "name": mst.commit.committer,
                    "datetime": mst.commit.committed_datetime
                }
            },
            "remote": {
                "author": {
                    "name": rmst.commit.author,
                    "datetime": rmst.commit.authored_datetime
                },
                "committer": {
                    "name": rmst.commit.committer,
                    "datetime": rmst.commit.committed_datetime
                }
            }
        }
        
        return info
