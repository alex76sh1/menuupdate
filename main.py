#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QWidget, QMainWindow, QApplication
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtCore import qDebug

from MainWindow_2 import MainWindow

def readProjects(filename = "./projects.json"):
    import json
    import codecs
    fl = codecs.open(filename, 'r', 'utf-8')
    return json.loads(fl.read())

if __name__ == '__main__':

    app = QApplication(sys.argv)
    win = MainWindow()
    win.setProjects(readProjects())
    win.show()
    sys.exit(app.exec_())
