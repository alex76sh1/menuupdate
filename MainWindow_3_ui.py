# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'MainWindow_3.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.lstProjectName = QtWidgets.QListWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lstProjectName.sizePolicy().hasHeightForWidth())
        self.lstProjectName.setSizePolicy(sizePolicy)
        self.lstProjectName.setObjectName("lstProjectName")
        self.horizontalLayout.addWidget(self.lstProjectName)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.cmbProjectVersion = QtWidgets.QComboBox(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(2)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cmbProjectVersion.sizePolicy().hasHeightForWidth())
        self.cmbProjectVersion.setSizePolicy(sizePolicy)
        self.cmbProjectVersion.setObjectName("cmbProjectVersion")
        self.gridLayout.addWidget(self.cmbProjectVersion, 0, 0, 1, 1)
        self.lbProjectVersionInfo = QtWidgets.QLabel(self.centralwidget)
        self.lbProjectVersionInfo.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.lbProjectVersionInfo.setObjectName("lbProjectVersionInfo")
        self.gridLayout.addWidget(self.lbProjectVersionInfo, 1, 0, 1, 1)
        self.horizontalLayout.addLayout(self.gridLayout)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.pbStart = QtWidgets.QPushButton(self.centralwidget)
        self.pbStart.setObjectName("pbStart")
        self.horizontalLayout_2.addWidget(self.pbStart)
        self.pbUpdate = QtWidgets.QPushButton(self.centralwidget)
        self.pbUpdate.setObjectName("pbUpdate")
        self.horizontalLayout_2.addWidget(self.pbUpdate)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.lbProjectVersionInfo.setText(_translate("MainWindow", "TextLabel"))
        self.pbStart.setText(_translate("MainWindow", "Запуск"))
        self.pbUpdate.setText(_translate("MainWindow", "Обновить"))

