from PyQt5.QtWidgets import QWidget, QMainWindow, QApplication
import MainWindow_3_ui as MainWindow_ui
from UpdateProject import ProjectDir

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        #self.ui = MainWindow_ui.Ui_MainWindow()
        self.ui.setupUi(self)
        self.setupConnect()
    ui =  MainWindow_ui.Ui_MainWindow()
    def setupConnect(self):
        self.ui.pbStart.clicked.connect(self.run)
        self.ui.pbUpdate.clicked.connect(self.update)
        self.ui.lstProjectName.currentRowChanged.connect(self.chooseProject)
        #self.ui.cmbProjectVersion.currentIndexChanged.connect(self.chooseVersion)
        
    def setProjects(self, lst):
        self.Projects = lst
        print("Projects:", lst)
        for p in lst:
            self.ui.lstProjectName.addItem(p["имя"])
        #self.chooseProject(ind=0)
            
    def chooseProject(self, ind=None, name=None):
        lstV = self.ui.cmbProjectVersion
        lstV.clear()
        
        if ind==None:
            ind=-1
            for p in self.Projects:
                ++ind
                if name == p["имя"]:
                    break;
        print("chooseProject ind:", ind)
        self.CurrentProject = self.Projects[ind]
        proj = self.CurrentProject 
        for v in proj["versions"]:
            lstV.addItem(v)
            
        self.m_ProjectDir = ProjectDir("./projects/"+ proj["name"], proj["url"])
        self.chooseVersion(0)
        
    def chooseVersion(self, ind):
        branch = self.ui.cmbProjectVersion.itemText(ind)
        print("Choose Version:", branch)
        if self.m_ProjectDir:
            self.printInfo(self.m_ProjectDir.getBranchInfo(branch))

    def printInfo(self, info):
        #print("printInfo:", info)
        txt = "LOL"
        if info:
            if type(info) is dict: 
                locale = info["locale"]
                author = locale["author"]
                txt_author = "Автор: %s\n\tДата: %s" % (author["name"], author["datetime"]) 
                committer = locale["committer"]
                txt_committer = "Коммитер: %s\n\tДата: %s" % (committer["name"], committer["datetime"]) 
                txt_locale = txt_author + "\n\n" + txt_committer
                remote = info["remote"]
                author = remote["author"]
                txt_author = "Автор: %s\n\tДата: %s" % (author["name"], author["datetime"]) 
                committer = remote["committer"]
                txt_committer = "Коммитер: %s\n\tДата: %s" % (committer["name"], committer["datetime"]) 
                txt_remote= txt_author + "\n\n" + txt_committer
                
                txt = txt_locale + "\n------------\n" + txt_remote
                
                if info["locale"]["committer"]["datetime"] < info["remote"]["committer"]["datetime"]:
                    self.ui.pbUpdate.setText("Download")
                else: self.ui.pbUpdate.setText("Update")
                
            elif type(info) is str: txt = "Info: " + info
        self.ui.lbProjectVersionInfo.setText(txt)

    def update(self):
        print("Update of", self.CurrentProject["name"] )
        from PyQt5.QtWidgets  import QApplication as app
        self.ui.pbUpdate.setEnabled(False)
        self.ui.lbProjectVersionInfo.setText("Получение информации")
        app.processEvents()
        if self.m_ProjectDir:
            b = False
            if self.ui.pbUpdate.text() == "Update": 
                b = self.m_ProjectDir.update()
            elif self.ui.pbUpdate.text() == "Download": b = self.m_ProjectDir.download()
            if b: 
                branch = self.ui.cmbProjectVersion.currentText()
                self.printInfo(self.m_ProjectDir.getBranchInfo(branch))
            else: self.ui.lbProjectVersionInfo.setText("Интернет отсутствует!")
        self.ui.pbUpdate.setEnabled(True)
    
    def run(self):
        projectName = self.CurrentProject["name"]
        print("Run is ", projectName)
        projectDir = "./projects/"+projectName+"/"
        import os
        pwd = os.getcwd()
        print("Os name:", os.name)
        if os.name == "nt":
            self.setupEnvWindows()
            os.chdir(projectDir)
            os.system("qbs.exe run config:release profile:qtc_Desktop__595e164e")
            os.chdir(pwd)

    def setupEnvWindows(self):
        import os
        os.environ["PATH"] = "C:\\Programming\\Qt_4\\5.12.0\\mingw73_64\\lib;C:\\Programming\\Qt_4\\Tools\\mingw730_64\\bin;C:\\Programming\\Qt_4\\5.12.0\\mingw73_64\\bin;C:\\Programming\\Qt_4\\Tools\\QtCreator\\bin;"
        os.environ["QTDIR"] = "C:\\Programming\\Qt_4\\5.12.0\\mingw73_64"
        
